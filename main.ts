function convertImgToBase64(url: string, outputFormat: string = null): Promise<string> {
  return new Promise<string>(resolve => {
      var img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = function () {
          var canvas = document.createElement('canvas');
          var ctx = canvas.getContext('2d');
          canvas.height = img.height;
          canvas.width = img.width;
          ctx.drawImage(img, 0, 0);
          var dataURL = canvas.toDataURL(outputFormat || 'image/png');
          resolve(dataURL);
          canvas = null;
      };
      img.src = url;
  })
}

function delay(delay: number = 100): Promise<void>{
  return new Promise(resolve => setTimeout(resolve, delay))
}

function mouseDown(element: Element){
  let event = new MouseEvent('mousedown', {
    modifierAltGraph: true,
    bubbles: true,
    relatedTarget: element
  })

  window.dispatchEvent(event)
  document.dispatchEvent(event)
  element.dispatchEvent(event)
}
function mouseClick(element: Element){
  let event = new MouseEvent('click', {
    modifierAltGraph: true,
    bubbles: true,
    relatedTarget: element
  })

  window.dispatchEvent(event)
  document.dispatchEvent(event)
  element.dispatchEvent(event)
}

async function getDom(query: string, d: Element | Document = document): Promise<Element>{
  let get = d.querySelector(query)

  while(!get){
    await delay(100)
    get = d.querySelector(query)
  }

  return get
}

async function getDomAll(query: string, d: Element | Document = document): Promise<NodeList>{
  let get = d.querySelectorAll(query)

  while(!get.length){
    await delay(100)
    get = d.querySelectorAll(query)
  }

  return get
}

async function main(){
  let panes = await getDomAll('.pane')
  let menus = await getDomAll('.menu-horizontal-item')

  let background = document.createElement('div')
  let persent = document.createElement('p')

  let bs = background.style
  bs.width = '100%'
  bs.height = '100%'
  bs.position = 'fixed'
  bs.top = '0'
  bs.left = '0'
  bs.right = '0'
  bs.bottom = '0'
  bs.background = 'rgba(0,0,0,0.8)'
  bs.opacity = '0'
  bs.transitionDuration = '0.2s'
  bs.zIndex = '-100'
  bs.margin = 'auto'

  background.appendChild(persent)
  document.body.appendChild(background)

  let newChat: HTMLDivElement = <HTMLDivElement>menus[1]
  let newChatButton = newChat.querySelector('button')

  let exportElement = document.createElement('div')

  exportElement.className = 'menu-horizontal-item'
  exportElement.innerHTML = `
  <button title="Экспортировать контакты">
    <span data-icon="status-v3" class="">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path transform="translate(-2 -2)" fill="#727A7E" xmlns="http://www.w3.org/2000/svg" d="M22.8,12.29a4.66,4.66,0,0,1-1.79,9H18.44l1.51-1.8a2.31,2.31,0,0,0-1.82-3.89h-1V12.91a2.83,2.83,0,0,0-2.82-2.82h-1a2.83,2.83,0,0,0-2.82,2.82v2.64h-1a2.31,2.31,0,0,0-1.82,3.89l1.51,1.8H6.7a4.77,4.77,0,0,1-2.36-8.87A3.25,3.25,0,0,1,9.43,8.85a7,7,0,0,1,13.4,3c0,0.14,0,.28,0,0.42m-4.67,5.23h-3V12.91a0.85,0.85,0,0,0-.85-0.85h-1a0.85,0.85,0,0,0-.85.85v4.61h-3c-0.47,0-.61.29-0.31,0.65l4.12,4.92a0.69,0.69,0,0,0,1.09,0l4.12-4.92c0.3-.36.16-0.65-0.31-0.65" />
      </svg>
    </span>
  </button>
  <span></span>
  `
  newChat.parentElement.insertBefore(exportElement, menus[0])

  exportElement.addEventListener('mousedown', async () => {
    let elem = <HTMLElement>panes[0]
    elem.style.opacity = '0';
    bs.zIndex = '999'
    await delay(1)
    bs.opacity = '1'
    await delay(200)
    mouseDown(newChatButton)
    let body = await getDom('.drawer-body', elem)
    let list = await getDom('.infinite-list-viewport', body)
    let back = await getDom('.btn-close-drawer', elem)
    let images: string[] = []
    let usersObjects: any = {}
    let rePhone = /u=([0-9]+)/

    let t: any = {}

    Object.defineProperty(t, 'top', {get: () => body.scrollTop})
    Object.defineProperty(t, 'height', {get: () => body.scrollHeight - body.clientHeight})
    Object.defineProperty(t, 'pers', {get: () => Math.floor(t.top / t.height * 100)})

    persent.innerText = 'Прогресс: 0/100'
    while(t.top < t.height){
      persent.innerText = `Прогресс: ${t.pers}/100`
      await delay(100)
      body.scrollTop += body.clientHeight / 2
      let users = await getDomAll('.chat.contact', list)

      for(let i = 0; i < users.length; i++){
        let user: HTMLDivElement = <HTMLDivElement>users[i]
        let img: HTMLImageElement = <HTMLImageElement>user.querySelector('.avatar-image')

        if(img && img.src){
          let phone

          if(img && (phone = rePhone.exec(img.src))){

            if(phone.length > 1 && !usersObjects[phone[1]]){
              let title: HTMLElement = <HTMLElement>user.querySelector('.chat-title')
              usersObjects[phone[1]] = {}
              usersObjects[phone[1]].phone = phone[1]
              usersObjects[phone[1]].image = await convertImgToBase64(img.src.replace('t=s', 't=l'))
              usersObjects[phone[1]].name = title.innerText.trim()
            }
          }
        }
      }
      persent.innerText = `Прогресс: ${t.pers}/100`
    }
    persent.innerText = `Прогресс: ${t.pers}/100`
    mouseClick(back)
    elem.style.opacity = '1';
    bs.opacity = '0'
    await delay(200)
    bs.zIndex = '-100'
    await delay(1)

    // Собсна вот тут объект
    usersObjects
  })
}

window.addEventListener('load', () => main().catch(console.error))
